## Downloading the data and scripts

Don't feel the need to download the data (or clone the entire repository). The scripts use the GitLab URL to access the data.